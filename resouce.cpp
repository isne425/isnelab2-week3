#include<cstdlib>
#include<ctime>

class customer
{
public :
	customer() {} // construction

	// attributes
	int get_arrTime() const { return arrTime; }
	int get_serTime() const { return serTime; }

	// random arrTime & serTime method
	customer(int minSerTime, int maxSerTime)
	{
		int diff = maxSerTime - minSerTime; // service time diff
		arrTime = rand() % 241; // arrival time = 0-240 mins
		serTime = (rand() % diff) + minSerTime; // calculate service time
	}


	customer(const customer &cus)
	{
		arrTime = cus.get_arrTime();
		serTime = cus.get_serTime();
	}

	friend bool operator <(customer a, customer b) 
	{
		if (a.arrTime < b.arrTime) { return true; }
		else { return false; }
	}

	customer operator =(customer& rhs) 
	{
		arrTime = rhs.get_arrTime();
		serTime = rhs.get_serTime();
		return *this;
	}

private:
	int arrTime;
	int serTime;

};